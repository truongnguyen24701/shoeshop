import React, { Component } from 'react'


// shoeData
export default class Item_Shoe extends Component {
    render() {
        let { name, description, image } = this.props.shoeData;
        return (
            <div className='col-3'>
                <div className="card" style={{ width: "100%" }}>
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{description.length < 40
                            ? description
                            : description.slice(0, 40) + "..."}</p>
                        <button
                            onClick={() => {
                                this.props.handleAddToCard(this.props.shoeData)
                            }}
                            className="btn btn-primary">Add to cart</button>
                    </div>
                </div>

            </div>
        )
    }
}
