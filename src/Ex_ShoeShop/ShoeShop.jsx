import React, { Component } from 'react'
import { shoeArr } from './Data_ShoeShop'
import Item_Shoe from './Item_Shoe';
import TableGioHang from './TableGioHang';

export default class ShoeShop extends Component {
    state = {
        shoeArr: shoeArr,
        gioHang: [],
    };

    // cập nhập giỏ hàng thêm sản phẩm
    renderShoe = () => {
        return this.state.shoeArr.map((item) => {
            return <Item_Shoe
                handleAddToCard={this.handleAddToCard}
                shoeData={item}
                key={item.id} />
        })
    }


    // cập nhập giỏ hàng tăng giảm số lượng
    handleAddToCard = (sp) => {
       
        let index = this.state.gioHang.findIndex((item) => {
            return item.id == sp.id;
        });
        let cloneGioHang = [...this.state.gioHang,]

        if (index == -1) {
            //th 1
            let newSp = { ...sp, soLuong: 1 }
            cloneGioHang.push(newSp)
        } else {
            //th2
            cloneGioHang[index].soLuong++
        }
        this.setState({
            gioHang: cloneGioHang,
        })
    };

    handleUpdate = (number, id) => {
        let data = this.state.gioHang.find((item) => {
            return item.id == id;
        });
        if (data) {
            if (data.soLuong >= 1) {
                if (number == -1 && data.soLuong == 1) {
                    alert ("Vui lòng click chuột vào nút xoá!! xin cảm ơn")
                    return 0
                }
                data.soLuong += number 
                const newGioHang = this.state.gioHang
                
                newGioHang.map(item => {
                    if(item.id == data.id) {
                        item = data
                    }
                })

                this.setState({
                    gioHang: newGioHang,
                })
            }
       }


    };


    //xoá sản phẩm
    handleRemoveShoe = (idShoe) => {
        let index = this.state.gioHang.findIndex((item) => {
            return (item.id = idShoe)
        })
        if (index == -1) {
            let cloneGioHang = [...this.state.gioHang]
            cloneGioHang.splice(index, 1)
            this.setState({
                gioHang: cloneGioHang
            })
        }
    }

    // thêm nút click cộng sản phẩm lên 1


    render() {
        return (
            <div className='container py-5'>
                {this.state.gioHang.length > 0 && ( // thêm số lượng sản phẩm
                    <TableGioHang
                    handleUpdate= {this.handleUpdate}
                        handleRemoveShoe={this.handleRemoveShoe}
                        gioHang={this.state.gioHang} />
                )}
                <div className='row'>{this.renderShoe()}</div>
            </div>
        )
    }
}
