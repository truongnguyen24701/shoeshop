import React, { Component } from 'react'

export default class TableGioHang extends Component {
    renderTable = () => {
        return this.props.gioHang.map((item) => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                        <button onClick={() => this.props.handleUpdate(-1, item.id)} className='btn btn-primary'> - </button>
                        <span className='mx-2'>{item.soLuong}</span>
                        <button onClick={() => this.props.handleUpdate(1, item.id)} className='btn btn-dark'> + </button>
                    </td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleRemoveShoe()
                            }}
                            className='btn btn-danger'>Xoá</button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTable()}
                    </tbody>
                </table>
            </div>
        )
    }
}
